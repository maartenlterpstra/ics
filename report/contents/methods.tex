%!TEX root = ../report.tex
\section{Methods}
In this section, we will describe the model that we used for the inverted pendulum.

\subsection{Spring-mass model}
In \cref{fig:inverted_pendulum_fig}, there is only one mass at the end of the rod.
However, this is not entirely realistic; this means that the mass of the pendulum is centered at only that point.
Moreover, this would also mean that the supporting rod is massless.
As we can observe in reality, this is impossible to reproduce. 
For the rest of this report, we will use our spring-mass model of the pendulum, which is slightly more realistic.

In the spring-mass model, we will have multiple point masses which are connected with springs.
With this model, the shape of the system has an influence on the center-of-mass.
Our model is illustrated in \cref{fig:inverted_pendulum_model}.
The particles -- each of which has a mass of \SI{20}{\gram} --  are connected with springs, and there is a driven particle that has a deterministic position according to \(y(t) = A\cos(2\pi ft)\), for some given amplitude \(A\) (in cm) and frequency \(f\) (in Hz).
The springs that connect the particles have a large spring constant, which makes them stiff.

\begin{figure}[htbp!]
	\centering
	\firstpendulum
	\caption{
		The inverted pendulum with a driven base.
		The black dots at the corners visualize the masses of the system.
		The red dot at the base is the driven particle.
		The black lines are the springs that connect the particles.
	}
	\label{fig:inverted_pendulum_model}
\end{figure}

\subsubsection{Shortcomings of the spring-mass model}
The rod has been modelled with four particles that are connected with stiff springs, as can be seen in \cref{fig:inverted_pendulum_model}.
The main goal of a spring-mass system is to gain realism when compared to the model where the mass is simulated as a single point-mass.
However, inverted pendula in the real world have a solid body -- consider e.g. an aluminium rod.
Ideally, these rigid bodies should not be malleable.\footnote{Of course, on a smaller scale, physical `rigid' objects are not really rigid, but are more similar to a spring-mass system with many masses, and springs with very high spring constants. But it is fair to make the rigid body assumption on this scale, considering computational feasibility.}
A spring-mass system inherently \emph{is} malleable as springs are flexible making these systems also less realistic.

To circumvent this, flexible springs need to be replaced with inflexible counterparts.
One way to do this is to use springs with very large constants, and increase them even more if necessary.
However, this has a tremendous downside:
To achieve acceptable accuracy, the time step in the simulation is determined by the highest frequency present in the simulation\footnote{This includes all spring frequencies \emph{and} the (user-specified) frequency of the driven particle.}, with the following heuristic:
\[
	\Delta t \lesssim \frac{
		1
	}{
		20 \cdot f_{\text{max}}
	}.
\]
Springs have a natural frequency which is approximated by \(f_n = \frac{1}{2\pi}\sqrt{\frac{k}{m}}\).
Since we want the springs to behave like rigid bonds, their spring constants should be large.
This results in a large natural frequency, and ultimately to a very small time step for reasonable accuracy.
A small time step is not desirable, and therefore, simulating rigid bonds in a different way may be preferable.

\subsection{Rigid body simulations}
We have established that we cannot realistically simulate a rigid body in a computationally efficient manner using stiff springs.
Therefore, another way needs to be found. 
From a user's point of view, a rigid body is an object which has as a property that for all points in an in object, the distance to every other point remains constant over time.
This property can lead to an obvious implementation strategy: impose explicit constraints between each pair of particles which have all to be satisfied at all times.
This constraint can have many forms: 
\begin{itemize}
	\item It can try to keep the distances constant between pairs of particles
	\item The relative locations of the centres of masses can remain constant
	\item The perimeter and angles of triangles formed by triplets of particles remains constant
	\item ...
\end{itemize}
Of course, this does not directly lead to an efficient implementation of such algorithms.
More so, the general Constraint Satisfaction Problem is NP-Complete \cite[Section 4E]{yokoo1998distributed}.
However, we are not trying to solve the general problem but we \href{pun_intended.jpg}{constrain} ourselves to only five particles.
One possible way to solve this is by using Lagrange multipliers, which we will look at next.

\subsubsection{Lagrange multipliers}
A common way to solve rigid constraints is to use Lagrange multipliers.
In this method, the \(n\) constraints are specified as the following \(n\) equations:
\[
	\sigma_k(t) \equiv \norm{\vec{x}_{ka}(t) - \vec{x}_{kb}(t)}^2 - d_k^2 = 0
	\text{ for } k = 1 \ldots n,
\]
where \(\vec{x}_{ka}\) and \(\vec{x}_{kb}\) are the positions of the particles that are involved in constraint \(k\).
\(d_k\) is the bond length of the constraint.

The unconstrained acceleration equations of motion of the system are given by:
\[
	\frac{
		\partial^2\vec{x}_i(t)
	}{
		\partial t^2
	}
	m_i = 
	-\frac{
		\partial
	}{
		\partial \vec{x}_i
	}
	V(\vec{x}_i(t))
	\text{ for } i = 1 \ldots N,
\]
where \(N\) is the number of particles in the system.

To correct for the constraints, terms are added to the energy landscape:
\begin{align*}
	\frac{
		\partial^2\vec{x}_i(t)
	}{
		\partial t^2
	}
	m_i &= 
	-\frac{
		\partial
	}{
		\partial \vec{x}_i
	}
	\left(
		V(\vec{x}_i(t)) + \sum\limits_{k=1}^n \lambda_k\sigma_k(t)
	\right) \\
	\text{ for } i &= 1 \ldots N.
\end{align*}
As can be seen, if a constraint \(k\) is satisfied, no term is added to the particles involved (since \(\sigma_k(t) = 0\) if the constraint is satisfied.).
The difficulty in the Lagrange multiplier method lies in the determination of the Lagrangian multipliers \(\lambda_k\).

For finding the Lagrange multipliers there are several methods, like SHAKE\cite{ryckaert1977numerical}, RATTLE\cite{andersen1983rattle}, MILC SHAKE\cite{bailey2008efficient}, LINCS\cite{hess1997lincs}.
We will not go into details about most of these methods, but have listed them here for reference.

\subsubsection{SHAKE}
One of the algorithms that is used to satisfy these constraints simultaneously using Lagrange multipliers is SHAKE.
In our implementation, we have not considered the Lagrange multipliers, but have used a geometrical interpretation of the method.

\cref{alg:shake} shows the pseudocode for our iterative implementation of SHAKE.
Essentially, the algorithm iteratively enforces the constraints, until all constraints are satisfied to a reasonable\footnote{We used \(\frac{\abs{d - d'}}{d} < 10^{-4}\), where \(d\) is the bond length, and \(d'\) is the actual distance between the particles.} precision.

Moreover, the enforcement of a constraint is done by moving the particles that are involved in the constraint along the line of the constraint.
The particles are moved in equal magnitude, but opposite direction, as shown in \cref{fig:constraint_enforcement}.

Of course, the situation in \cref{fig:constraint_enforcement} is trivial since it is an isolated constraint.
Coupled constraints can be enforced by using \cref{alg:shake}.
In general, this works well, but if a high precision is required, many iterations (several hundreds) may be needed before the constraints are fulfilled.

\begin{algorithm}
	\begin{algorithmic}
		\State Perform unconstrained update on particles
		\While{not all constraints satisfied}
			\For{all constraints \(c_i \in C\)}
				\State Enforce \(c_i\)
			\EndFor
		\EndWhile
	\end{algorithmic}
	\caption{Iterative constraint enforcement}
	\label{alg:shake}
\end{algorithm}

\begin{figure}
	\includegraphics[width=\columnwidth]{images/simple_constraint.eps}
	\caption{Enforcing an isolated constraint}
	\label{fig:constraint_enforcement}
\end{figure}