%!TEX root = ../report.tex
\section{Introduction}
The pendulum is one of the earliest physical systems that has been studied.
It was notably studied by Galileo Galilei, Christiaan Huygens and Isaac Newton and has been the most accurate way to track time for more than 300 years.

Many people are familiar with the pendulum from day-to-day objects such as clocks and the studies of its properties during high school physics, e.g. the fact that the period of oscillation is \(T = 2\pi\sqrt{\frac{L}{g}}\).
Essentially, a pendulum consists of a mass that is attached to a fixed point with some sort of bond.
Depending on the application, this bond can be either a spring, rope, or a rigid bond.

What most people will also recall from high school, is that a pendulum has merely one stable point: downwards, below the pivot point.
Due to gravity, a regular pendulum will stabilize towards the floor, even when nudged away from it; it is a stable equilibrium.
It turns out that the pendulum can be an even more interesting system when some of its properties are changed.

\subsection{Inverted pendulum}
An inverted pendulum is very similar to a regular pendulum, except for the fact that it has been inverted (as the name suggests).
This means that the mass is directly \emph{above} the pivot point.
It is intuitive to assume that this orientation is unstable, as most people would expect it to fall down due to gravity.
However, this is a much studied topic in control theory; controlling a cart or a platform to keep a mass above the pivot point even when it is perturbed.
This type of pendulum is often used as model for human walk or as a simulation of vehicles such as Segways.
In many control systems, the mass is kept above the pivot point by moving a platform in the horizontal direction.

In this report, however, we will focus on a specific type of inverted pendulum, called Kapitza's pendulum.
This type of inverted pendulum is mostly the same as a regular pendulum, except that the base is mounted on a vertically oscillating platform, which allows the pendulum to be stable in the inverted orientation.
An illustration of this system can be seen in \cref{fig:inverted_pendulum_fig}.
This type of inverted pendulum was first described by A. Stephenson \cite{stephenson1908xx}.
However, it took until 1951 until P. L. Kapitza gave a mathematical explanation to \emph{why} this interesting behaviour occurs \cite{kapitza1951dynamic}.
In this report, we describe how Kapitza's pendulum operates and how it can be simulated on a computer.

\begin{figure}[htbp!]
	\centering
	\invpendulum[1.5]
	\caption{The inverted pendulum with a driven base.}
	\label{fig:inverted_pendulum_fig}
\end{figure}

\section{Kapitza's pendulum}
For completeness, we now reproduce the same equations that Kapitza has derived.

\subsection{Equations of motion and stability}
\label{sec:stability}
To acquire the equations of motion of this system in terms of \(\theta\), we can first write the Cartesian coordinates of the mass of the pendulum in terms of \(\theta\):
\[
	(x, y) =
	(\ell \sin\theta, y_0 + \ell \cos\theta),
\]
where \(y_0(t) = A\cos(\omega t)\) is the oscillatory motion of the base as a function of time.

To acquire the kinetic energy of the system, we need the squared speed of the mass:
\[
	v^2 = \dot x^2 + \dot y^2,
\]
where \(\dot x\) and \(\dot y\) are the time derivatives of the \(x\)- and \(y\)-coordinates:

\[
	\begin{cases}
		\dot x = \ell\dot\theta\cos\theta	\\
		\dot y = \dot y_0 - \ell\dot\theta\sin\theta.
	\end{cases}
\]

Hence, the kinetic energy is given by:
\[
	E_k = \frac{1}{2} m v^2 = \frac{1}{2}m(\dot y_0^2 + \ell^2\dot\theta^2 - 2\ell\dot y_0\dot\theta\sin\theta)
\]

Moreover, the potential energy is given by the gravitational energy:
\[
	E_p = mgy = mg(y_0 + \ell\cos\theta)
\]

To get the equations of motion, we use the Euler-Lagrange equations:
\[
	\frac{d}{dt}\left(
		\frac{\partial\mathcal{L}}{\partial\dot\theta}
	\right) =
	\frac{\partial\mathcal{L}}{\partial\theta},
\]
with \(\mathcal{L} = E_k - E_p\).
Filling everything in, this reduces to:
\[
	\ell\ddot\theta + \sin\theta\left(A\omega^2\cos(\omega t) - g\right) = 0
\]

This second order ODE is difficult to solve, because of the time-dependent term \(A\omega^2\cos(\omega t)\).

In \cite[pp. 258]{morin2008introduction}, it is argued that small deviations around \(\theta = 0\) at \(t=0\) results in a motion back to \(\theta = 0\), only if
\begin{equation}
	\label{eq:necessary}
	A \ll \ell \text{ and } A\omega^2 \gg g.
\end{equation}
That is, only if the above condition is fulfilled (to a proper degree), the inverted pendulum can be stable.
Note that this condition is necessary but not sufficient for stability of the inverted pendulum.

\subsection{Intuitive understanding}
The stability of Kapitza's pendulum can be intuitively explained by considering a small perturbation around \(\theta = 0\), and trying to picture what happens if at that moment the oscillation accelerates downwards while moving upward: The mass gets swung \emph{towards} \(\theta = 0\).
If the frequency is chosen properly, the oscillation accelerates upwards when the mass swings past \(\theta = 0\), and downwards again when the perturpation is on the other side of \(\theta = 0\).