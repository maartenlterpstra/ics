#!/bin/python
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri
data = np.load('stabdata.npz')

fig = plt.figure()

stable_freqs = data["freq_n"][data["stables"]]
stable_amps = data["amplitudes"][data["stables"]]
unstable_freqs = data["freq_n"][np.invert(data["stables"])]
unstable_amps = data["amplitudes"][np.invert(data["stables"])]

triang_s = tri.Triangulation(stable_freqs, stable_amps)
triang_u = tri.Triangulation(unstable_freqs, unstable_amps)

print(list(triang_s))
plt.triplot(triang_u, 'ro-')
plt.triplot(triang_s, 'bo-')
plt.xlabel('frequency (Hz)')
plt.ylabel('amplitude (cm)')
plt.axis((0, 600, 0, 0.5))
plt.savefig('triangulate.eps')
plt.figure()
plt.scatter(unstable_freqs, unstable_amps, color='red')
plt.scatter(stable_freqs, stable_amps, color='blue')
plt.xlabel('frequency (Hz)')
plt.ylabel('amplitude (cm)')
plt.axis((0, 600, 0, 0.5))
plt.savefig('scatter.eps')
plt.show()
