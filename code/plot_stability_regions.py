#!/bin/python
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

if __name__ == '__main__':
    plot_data = np.load('stabdata-zeus-big.npz')

    freqs = plot_data['freq_n']
    amps = plot_data['amplitudes']
    stables = plot_data['stables']
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title('Stability')
    ax.set_xlim([min(freqs), max(freqs)])
    ax.set_ylim([min(amps), max(amps)])
    ax.set_xlabel(r'f (Hz)')
    ax.set_ylabel(r'A (cm)')
    colors = [[0, 0, 0] if s else [1, 1, 1] for s in stables]
    [ax.add_artist(Rectangle(xy=pair, color=colors[i], width=1, height=max(amps) / len(np.unique(amps)))) for (i, pair) in enumerate(zip(freqs, amps))]
    plt.savefig('zeus-big.png')
    plt.savefig('zeus-big.eps', format='eps', dpi=1200)

    fig2 = plt.figure()
    ax = fig2.add_subplot(111)
    ax.set_title('Necessary stability conditions')
    ax.set_xlim([min(freqs), max(freqs)])
    ax.set_ylim([min(amps), max(amps)])
    ax.set_xlabel(r'f (Hz)')
    ax.set_ylabel(r'A (cm)')

    data = ((x, y, [0, 0, 0]) if (y / 100) * (x / (2 * np.pi)) ** 2 > 0.02 * 9.81 else (x, y, [1, 1, 1]) for x in np.unique(freqs) for y in np.unique(amps))
    for (x, y, color) in data:
        ax.scatter(x, y, marker='o', color=color)
    plt.show()
