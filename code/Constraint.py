import numpy as np
from OpenGL.GL import *

from Particle import Particle
from DrivenParticle import DrivenParticle


class Constraint:

    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
        self.r = np.linalg.norm(self.p1.position - self.p2.position)

    def enforce(self, dt):
        # The distance vector between the points
        d1 = self.p2.position - self.p1.position

        # Unit vector in the direction that the particles are allowed to move.
        # (p2 in minus this direction)
        d1_norm = d1 / np.linalg.norm(d1)

        # The difference between the bond length and the actual distance
        d3 = (np.linalg.norm(d1) - self.r)

        # Add correction to enforce this constraints. Also to the velocity,
        # in retrospect, since otherwise it would not have been realistic.
        # For the driven particles, they cannot move, so the free particle
        # should be corrected with the entire difference.
        if type(self.p1) != DrivenParticle and type(self.p2) == DrivenParticle:
            self.p1.position += d1_norm * d3
            self.p1.velocity += d1_norm * d3 / (0.5 * dt)
        elif type(self.p1) == DrivenParticle and type(self.p2) != DrivenParticle:
            self.p2.position -= d1_norm * d3
            self.p2.velocity -= d1_norm * d3 / (0.5 * dt)
        else:
            self.p1.position += 0.5 * d1_norm * d3
            self.p1.velocity += 0.5 * d1_norm * d3 / (0.5 * dt)
            self.p2.position -= 0.5 * d1_norm * d3
            self.p2.velocity -= 0.5 * d1_norm * d3 / (0.5 * dt)

    def is_satisfied(self, precision):
        d1 = self.p2.position - self.p1.position
        d2 = np.linalg.norm(d1)
        return abs(self.r - d2) / self.r < 10**-precision

    def draw(self):
        glVertex2f(self.p1.position[0], self.p1.position[1])
        glVertex2f(self.p2.position[0], self.p2.position[1])
