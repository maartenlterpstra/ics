from Particle import Particle
import numpy as np
import math


class DrivenParticle(Particle):

    def __init__(self, name, x, y, mass, frequency, amplitude):
        super().__init__(name, x, y, mass)
        self.initial_position = np.array([x, y])
        self.frequency = frequency
        self.amplitude = amplitude

    def update_position(self, time):
        # Position is a direct function of the time.
        self.position = self.initial_position + self.amplitude * np.array([0.0, 1.0]) * math.cos(2 * math.pi * self.frequency * time)
        self.velocity = self.amplitude * 2 * math.pi * self.frequency * np.array([0.0, -1.0]) * math.sin(2 * math.pi * self.frequency * time)
