import numpy as np
from OpenGL.GL import *

import MathUtils

from Particle import Particle
from DrivenParticle import DrivenParticle


class Spring:
    # p1 and p2 are the particles connected by the edge
    # k is the spring constant (see Hooke's Law)
    # l is the rest length of the spring

    def __init__(self, p1, p2, k, l, dampening=0.2):
        self.p1 = p1
        self.p2 = p2
        self.k = float(k)
        self.l = float(l)
        self.dampening = float(dampening)

    def exert_force(self):
        # Compute distance vector between nodes
        dist_vec = self.p2.position - self.p1.position
        # Unit vector, for the force direction
        dist_vec_unit = MathUtils.unitVec(dist_vec)
        # Euclidean distance, for the force magnitude
        dist = np.linalg.norm(dist_vec)

        # Determine force by using Hooke's law.
        spring_force = self.k * (dist - self.l) * dist_vec_unit
        # Distribute the spring forces. But don't exert any forces on driven
        # particles. This is a bit ugly.
        if type(self.p1) != DrivenParticle:
            self.p1.add_force(spring_force)
        if type(self.p2) != DrivenParticle:
            self.p2.add_force(-spring_force)

        # Compute relative velocity vector
        v_rel_vec = self.p2.velocity - self.p1.velocity

        # Compute dampening force
        dampeningForce = self.dampening * v_rel_vec

        # Distribute the dampening forces to slow down the particles. But don't
        # exert forces on driven particles. This is a bit ugly.
        if type(self.p1) != DrivenParticle:
            self.p1.add_force(dampeningForce)
        if type(self.p2) != DrivenParticle:
            self.p2.add_force(-dampeningForce)

    def get_frequency(self):
        m1 = self.p1.mass
        m2 = self.p2.mass
        return np.sqrt(self.k * ((m1 + m2) / (2 * m1 * m2))) / (2.0 * np.pi)

    def get_particle_distance(self):
        return np.linalg.norm(self.p1.position - self.p2.position)

    def draw(self):
        glVertex2f(self.p1.position[0], self.p1.position[1])
        glVertex2f(self.p2.position[0], self.p2.position[1])

    def __repr__(self):
        return "({0} @ {3}, {1} @ {4}) - {2}".format(self.p1, self.p2, self.l, self.p1.position, self.p2.position)
