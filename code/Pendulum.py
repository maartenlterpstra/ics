from OpenGL.GL import *
from OpenGL.GLUT import *

import math
import numpy as np
import random

from Particle import Particle
from DrivenParticle import DrivenParticle
from Spring import Spring

random.seed(42)


class Pendulum:

    def __init__(self, particles, springs, constraints=[]):
        self.particles = particles
        self.springs = springs
        self.constraints = constraints

        self.update_accelerations()

        self.thetas = [self.theta()]
        self.theta_dots = [self.theta_dot()]
        self.e_kinetics = [self.e_kinetic()]
        self.e_gravs = [self.e_grav()]
        self.iters = [0]

    # Return whether the pendulum is currently considered stable.
    # Stability is currently defined as having a (abs) theta of smaller than 45
    # degrees.
    def isStable(self):
        return abs(self.thetas[-1]) < 45

    # Return whether the pendulum was stabe for the time until now.
    # TODO: Make more efficient/reliable
    def was_stable(self):
        if any(abs(theta) > 45 for theta in self.thetas):
            return False

        return (self.thetas[-1] - self.thetas[0]) < 0

    # Update using Verlet Velocity integration
    def update(self, dt, time, precision=4):

        # 1. --- Calculate half-step velocities and update positions according to that ---

        for p in self.particles:
            if type(p) != DrivenParticle:
                p.velocity += 0.5 * p.acc * dt
                p.position += p.velocity * dt
            else:
                p.update_position(time)

        # Stochastically enforce the constraints!
        iters = 0
        while not all([c.is_satisfied(precision) for c in self.constraints]):
            iters += 1
            [c.enforce(dt) for c in self.constraints]
        # 2. --- Derive new forces (and thus accelerations), using mid-velocity and new position ---

        self.update_accelerations()

        # 3. --- Calculate the end-point of the velocity in this step with the computed accelerations  ---

        for p in self.particles:
            p.velocity += 0.5 * p.acc * dt

        # Retain useful values
        self.thetas.append(self.theta())
        self.iters.append(iters)
        self.theta_dots.append(self.theta_dot())
        self.e_kinetics.append(self.e_kinetic())
        self.e_gravs.append(self.e_grav())

    def update_accelerations(self):
        # Update springs (which distribute forces to particles)
        [spring.exert_force() for spring in self.springs]

        for p in self.particles:
            # Add additional gravity
            if type(p) != DrivenParticle:
                p.add_gravity()

            # Newtons law
            p.acc = p.force / p.mass

            # Reset the force to zero (for next timestep)
            p.force *= 0

    def display(self):
        # Draw the springs
        glLineWidth(1.0)
        glColor3f(0.0, 0.0, 0.0)
        glBegin(GL_LINES)
        for s in self.springs:
            s.draw()
        glEnd()

        # Draw the constraints
        glLineWidth(3.0)
        glColor3f(1.0, 0.0, 0.0)
        glBegin(GL_LINES)
        for c in self.constraints:
            c.draw()
        glEnd()

        # Draw the particles
        for p in self.particles:
            p.draw()

    def theta(self):
        up = (self.particles[2].position + self.particles[3].position) / 2
        down = (self.particles[0].position + self.particles[1].position) / 2
        if up[0] > down[0]:
            return (180 / math.pi) * abs(math.atan2((up - down)[0], (up - down)[1]))
        else:
            return -(180 / math.pi) * abs(math.atan2((up - down)[0], (up - down)[1]))

    # Return the angular velocity of the pendulum, based on the velocities of
    # the particles that are furthest from the pivot point.
    def theta_dot(self):
        up = (self.particles[2].position + self.particles[3].position) / 2

        # Rotate 90 degrees clockwise, and normalize
        tangent_unit = np.array([-up[1], up[0]]) / np.linalg.norm(up)
        velocity = (self.particles[2].velocity + self.particles[3].velocity) / 2

        # Project the velocity on the tangent direction to retrieve the velocity in that direction
        velocity_proj = velocity.dot(tangent_unit)

        radius = np.linalg.norm(up)
        return -(180 / math.pi) * velocity_proj / radius

    def e_kinetic(self):
        return sum([p.e_kinetic() for p in self.particles])

    def e_spring(self):
        return sum([s.e_spring() for s in self.springs])

    def e_grav(self):
        return sum([p.e_grav() for p in self.particles])
