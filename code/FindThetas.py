#!/bin/python
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing
from Simulation import Simulation
from itertools import product
from mpl_toolkits.mplot3d import Axes3D


def do_find(ampfreq):
    amp, freq = ampfreq
    simulation = Simulation(freq, amp, 0.07, True, 0.1, 2)
    (theta, stability) = simulation.start()
    print('Done frequency {0}: Theta: {1}: Amplitude: {3}: Stable: {2}'.format(
        freq, theta, stability, amp))
    return (freq, theta, amp, stability)

if __name__ == '__main__':
    lowlim = 0
    limit = 600
    # One point for every frequency
    freqs = np.linspace(lowlim, limit, limit - lowlim + 1)
    amplitudes = np.linspace(0, 0.5, 16)
    ampfreqs = list(product(amplitudes, freqs))
    pool = multiprocessing.Pool(processes=8)
    results = [pool.map(do_find, ampfreqs)]
    freq_n, thetas, amps, stabs = zip(*results[0])
    thetas = np.array(thetas)
    freq_n = np.array(freq_n)
    stabs = np.array(stabs)
    amps = np.array(amps)
    # final = [freq_n, thetas, amps, stabs]
    np.savez_compressed('stabdata.npz', freq_n=freq_n, thetas=thetas, amplitudes=amps, stables=stabs)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_trisurf(freq_n, amps, thetas)
    # plt.scatter(freqs[stabs], thetas[stabs], color='red')
    # plt.scatter(
    #     freqs[np.invert(stabs)], thetas[np.invert(stabs)], color='blue')
    # plt.xlabel('frequency (Hz)')
    # plt.ylabel('theta (deg)')
    # plt.savefig('plotje-normaal.eps')
    # plt.figure()
    # plt.xkcd()
    # plt.scatter(freqs[stabs], thetas[stabs], color='red')
    # plt.scatter(
    #     freqs[np.invert(stabs)], thetas[np.invert(stabs)], color='blue')
    # plt.xlabel('frequency (Hz)')
    # plt.ylabel('theta (deg)')
    # plt.savefig('plotje-xkcd.eps')

    plt.show()
