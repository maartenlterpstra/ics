import numpy as np

# Raise exception for all errors, so we can catch them.
np.seterr(all='raise')

# Return unit vector of 'vec'. If 'vec' is zero, returns unit vector in the
# x-direction.


def unitVec(vec):
    try:
        return vec / np.linalg.norm(vec)
    except FloatingPointError:  # Catches division by zero
        # Return arbitrary unit vector, since it is not defined.
        return np.array([1, 0])
