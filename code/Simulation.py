#!/bin/python
from OpenGL.GL import *
from OpenGL.GL.ARB import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
import numpy as np
import time
from argparse import ArgumentParser
import matplotlib.pyplot as plt

from Pendulum import Pendulum
from Particle import Particle
from DrivenParticle import DrivenParticle
from Spring import Spring
from Constraint import Constraint
from MathUtils import unitVec


class Simulation:

    def __init__(self, freq, ampl, damp, use_constraints, theta, stop_time, timestep=None, fps=60, headless=True):
        self.freq = freq
        self.ampl = ampl
        self.damp = damp
        self.use_constraints = use_constraints
        self.theta = theta
        self.stop_time = stop_time
        self.timestep = timestep
        self.fps = fps
        self.headless = headless

        self.times = [0]
        self.pendulum = self.init_pendulum()

        # If the timestep is not specified, set it based on the frequencies in
        # the model.
        if self.timestep is None:
            self.update_timestep()

    def start(self):
        if not self.headless:
            # Make openGL stuff
            self.init_glut()
            self.init_opengl()
            # Start GLUT's main loop.
            glutMainLoop()
        else:
            # Do simulation in a simple while loop
            # Keep running while the maximum number of timesteps is not reached,
            # and the pendulum is stable
            while self.pendulum.isStable() and (self.times[-1] < self.stop_time or self.stop_time <= 0):
                self.step()
            return (abs(self.pendulum.theta()), self.pendulum.was_stable())

    def init_pendulum(self):
        mass = 0.02

        length_sides = 6.0
        length_upper_lower = 0.8
        length_to_pivot_from_below = 0.0

        # Make the corner particles
        p1 = Particle(
            "Chuck", -0.5 * length_upper_lower, -length_to_pivot_from_below, mass)
        p2 = Particle(
            "Sarah", 0.5 * length_upper_lower, -length_to_pivot_from_below, mass)
        p3 = Particle("Morgan", -0.5 * length_upper_lower,
                      length_sides - length_to_pivot_from_below, mass)
        p4 = Particle("Lester", 0.5 * length_upper_lower,
                      length_sides - length_to_pivot_from_below, mass)

        # Make the driven particle
        p5 = DrivenParticle("Big Mike", 0, 0, mass, self.freq, self.ampl)

        # Put them in a list
        particles = [p1, p2, p3, p4, p5]

        # Rotate all particles anti-clockwise around the origin
        [p.rotate(self.theta) for p in particles]

        # Spring properties
        spring_constant = 1e5

        # Rest lengths between middle particle and the upper and lower particles,
        # respectively.
        rest_length_middle_down = np.linalg.norm(p1.position - p5.position)
        rest_length_middle_up = np.linalg.norm(p3.position - p5.position)

        # Make all the horizontal and vertical springs
        s1 = Spring(p1, p2, spring_constant, length_upper_lower, self.damp)
        s2 = Spring(p2, p4, spring_constant, length_sides, self.damp)
        s3 = Spring(p1, p3, spring_constant, length_sides, self.damp)
        s4 = Spring(p3, p4, spring_constant, length_upper_lower, self.damp)

        # Make all 'diagonal' springs
        s5 = Spring(
            p5, p1, spring_constant, rest_length_middle_down, self.damp)
        s6 = Spring(
            p5, p2, spring_constant, rest_length_middle_down, self.damp)
        s7 = Spring(p5, p3, spring_constant, rest_length_middle_up, self.damp)
        s8 = Spring(p5, p4, spring_constant, rest_length_middle_up, self.damp)

        springs = [s1, s2, s3, s4, s5, s6, s7, s8]

        constraints = []
        if self.use_constraints:
            c1 = Constraint(p1, p3)
            c2 = Constraint(p2, p4)
            c3 = Constraint(p1, p4)
            c4 = Constraint(p2, p3)
            constraints = [c1, c2, c3, c4]

        return Pendulum(particles, springs, constraints)

    # This function makes sure that GLUT schedules a redisplay.
    def redisplay(self, val):
        glutPostRedisplay()

    # Function for updating the timestep (based on a change in the system due to user input)
    # This sets the timestep to 1/20th of the smallest oscillation period in
    # the system.
    def update_timestep(self):
        # Get the eigenfrequencies of the springs
        frequencies = [s.get_frequency() for s in self.pendulum.springs]
        # Add the driving frequency to the list
        frequencies.append(self.freq)
        # Set timestep based on those frequencies.
        self.timestep = (1 / 20) * 1 / max(frequencies)

    # Function for keyboard input
    def keyboard(self, key, x, y):
        if key == b'm':
            for particle in self.pendulum.particles:
                particle.mass = min(particle.mass * 1.05, 0.25)
            print("Mass per particle: {0}".format(
                self.pendulum.particles[0].mass))
            self.update_timestep()
        elif key == b'n':
            for particle in self.pendulum.particles:
                particle.mass = max(particle.mass / 1.05, 1e-3)
            print("Mass per particle: {0}".format(
                self.pendulum.particles[0].mass))
            self.update_timestep()
        elif key == b'a':
            self.pendulum.particles[4].amplitude = min(
                self.pendulum.particles[4].amplitude * 1.05, 0.25)
            print("Amplitude: {0}".format(
                self.pendulum.particles[4].amplitude))
        elif key == b'z':
            self.pendulum.particles[4].amplitude = max(
                self.pendulum.particles[4].amplitude / 1.05, 1e-3)
            print("Amplitude: {0}".format(
                self.pendulum.particles[4].amplitude))
        elif key == b'f':
            self.pendulum.particles[4].frequency = min(
                self.pendulum.particles[4].frequency * 1.01, 1000)
            print("Frequency: {0}".format(
                self.pendulum.particles[4].frequency))
            self.update_timestep()
        elif key == b'v':
            self.pendulum.particles[4].frequency = max(
                self.pendulum.particles[4].frequency / 1.01, 10)
            print("Frequency: {0}".format(
                self.pendulum.particles[4].frequency))
            self.update_timestep()
        elif key == b'p':
            push_force = 1500 * \
                np.array(
                    unitVec(self.pendulum.particles[3].position - self.pendulum.particles[2].position))
            self.pendulum.particles[2].add_force(push_force)
            print("Pushed Morgan with {0} Newtons.".format(
                np.linalg.norm(push_force)))

    # GLUT display function
    def display(self):
        glClearColor(1.0, 1.0, 1.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT)
        self.pendulum.display()
        glFlush()
        # Reschedule redisplay so we get approximately 60fps.
        glutTimerFunc(int(1000 / self.fps), self.redisplay, 0)

    # Initialize GLUT
    def init_glut(self):
        glutInit([])
        glutInitWindowSize(600, 600)
        glutInitWindowPosition(10, 360)
        glutCreateWindow('Inverted pendulum simulation')
        glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_MULTISAMPLE)
        glutIdleFunc(self.step)
        glutKeyboardFunc(self.keyboard)
        glutDisplayFunc(self.display)
        # Make GLUT return control flow after closing window.
        glutSetOption(
            GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION)

    # Intitialize OpenGL
    def init_opengl(self):
        # Print some GPU information.
        print('OpenGL vendor: ', glGetString(GL_VENDOR).decode('utf-8'))
        print('OpenGL renderer: ', glGetString(GL_RENDERER).decode('utf-8'))
        print('OpenGL version: ', glGetString(GL_VERSION).decode('utf-8'))

        # Zoom in to the pendulum.
        glScalef(0.15, 0.15, 0)

    # Perform a step in the simulation. If the time limit is reached, GLUT is
    # told to stop its loop.
    def step(self):
        if not self.headless and (self.times[-1] > self.stop_time and self.stop_time > 0):
            glutLeaveMainLoop()
        self.pendulum.update(self.timestep, self.times[-1])
        self.times.append(self.times[-1] + self.timestep)

if __name__ == '__main__':
    parser = ArgumentParser(description='Do a pendulum simulation.')
    parser.add_argument('--timestep', '-ts', metavar='timestep', type=float, help='The timestep to use in the simulation. By default, the most optimal timestep is computed based on the system\'s frequencies')
    parser.add_argument('--time', '-t', metavar='time', default=0, type=float, help='The total time of the simulation. Zero means infinite. Defaults to zero.')
    parser.add_argument('--fps', metavar='fps', default=60, type=int, help='Desired number of frames per second for the OpenGL rendering. Defaults to 60.')
    parser.add_argument('--freq', metavar='frequency', default=80.0, type=float, help='Frequency of the driven particle.')
    parser.add_argument('--theta', metavar='theta', default=0.0, type=float, help='Initial angle (clockwise) of the pendulum. Defaults to 0.')
    parser.add_argument('--ampl', metavar='amplitude', default=0.07, type=float, help='Amplitude of the oscillation of the driven particle.')
    parser.add_argument('--damp', metavar='dampening', default=0.1, type=float, help='Dampening factor of the springs.')
    parser.add_argument('--use_constraints', action='store_true', default=False, help='Whether or not to enforce constraints.')
    parser.add_argument('--headless', action='store_true', help='Run it headles (without rendering).')
    parser.add_argument('-o', default='plot.eps', help='Output file for plot.')
    args = parser.parse_args()

    print('Arguments: ', args)
    print('Run \'Simulation.py -h\' for help.')
    if args.timestep is not None and args.timestep * 20 > 1 / args.freq:
        print('WARNING: timestep may be too large for the given frequency.')

    simulation = Simulation(args.freq, args.ampl, args.damp, args.use_constraints,
                            args.theta, args.time, args.timestep, args.fps, args.headless)
    simulation.start()

    # Plot something here
    fig = plt.figure()

    # plt.plot(simulation.times, simulation.pendulum.iters, label='SHAKE iterations')
    # plt.plot(simulation.times, simulation.pendulum.e_kinetics, label=r'$E_k$')
    # plt.plot(simulation.times, simulation.pendulum.e_gravs, label=r'$E_p$')
    # plt.plot(simulation.times, np.array(simulation.pendulum.e_kinetics) + np.array(simulation.pendulum.e_gravs), label=r'$E_{total}$')
    # plt.xlabel(r'$t$ (s)')
    # plt.ylabel(r'$E$ (cJ)')
    # plt.legend(loc='best')
    plt.scatter(simulation.pendulum.thetas, simulation.pendulum.theta_dots, color='black', s=1)
    plt.xlabel(r'$\theta$ (deg)')
    plt.ylabel(r'$\dot\theta$ (deg per second)')
    fig.savefig(args.o)
    # plt.show()
