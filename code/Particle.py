from OpenGL.GL import *

import numpy as np
import math


class Particle:

    def __init__(self, name, x, y, mass):
        self.name = name
        self.position = np.array([float(x), float(y)])
        self.mass = float(mass)
        self.velocity = np.array([0.0, 0.0])
        self.acc = np.array([0.0, 0.0])
        self.force = np.array([0.0, 0.0])

    # Move points downwards in the y-direction
    def add_gravity(self, g=981):  # g is in cm/s^s
        self.add_force(np.array([0.0, -1.0]) * self.mass * g)

    def add_force(self, force):
        self.force += force

    # Rotate theta degrees around the origin (clockwise).
    def rotate(self, theta):
        # Convert to radians
        theta *= -math.pi / 180
        x = self.position[0]
        y = self.position[1]
        self.position = np.array([x * math.cos(theta) - y * math.sin(theta), x * math.sin(theta) + y * math.cos(theta)])

    def draw(self, radius=0.1, circle_sections=16):
        glColor3f(0.0, 0.0, 0.0)
        glBegin(GL_POLYGON)
        for i in range(circle_sections):
            cosine = radius * \
                np.cos(i * 2 * np.pi / circle_sections) + self.position[0]
            sine = radius * \
                np.sin(i * 2 * np.pi / circle_sections) + self.position[1]
            glVertex2f(cosine, sine)
        glEnd()

    def e_kinetic(self):
        return 0.5 * self.mass * np.linalg.norm(self.velocity) ** 2

    def e_grav(self):
        return self.mass * 981 * self.position[1]

    def __repr__(self):
        return str('{0}'.format(self.name))
